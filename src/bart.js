const parseString = new require('xml2js').parseString;

const request = require('request');
class BartClient {

    constructor(kwargs) {
        this.options = {
            key: 'MW9S-E7SL-26DU-VV8V',
            apiHost: 'http://api.bart.gov'
        };
        Object.assign(this.options, kwargs);
    }

    _parseXML(xmlbody, successCallback, errorCallback) {
        parseString(xmlbody, {trim: true}, function (err, result) {
            if (err) errorCallback(err);
            else if (result.root.message[0].error) {
                errorCallback({
                    text: result.root.message[0].error[0].text[0],
                    details: result.root.message[0].error[0].details[0]
                });
            }
            else {
                successCallback(result.root);
            }
        });
    }

    /**
     * Get information on advisories
     * @param callback, function(err, stationDetail){}
     */
    advisories(callback) {
        const url = `${this.options.apiHost}/api/bsa.aspx?cmd=bsa&key=${this.options.key}`;
        request(url, (err, resp, body) => {
            if (err) callback(err);
            else {
                this._parseXML(body, xml => {
                    callback(err, xml.bsa.map(a => {
                        return {
                            description: a.description
                        }
                    }));
                }, handleErr => callback(handleErr));
            }
        });
    }

    /**
     *
     * @param callback, function(err, stationDetail){}
     */
    stations(callback) {
        const url = `${this.options.apiHost}/api/stn.aspx?cmd=stns&key=${this.options.key}`;
        request(url, (err, resp, body) => {
            if (err) callback(err);
            else {
                this._parseXML(body, xml => {
                    callback(err, xml.stations[0].station.map(st => {
                        return {
                            name: st.name[0],
                            abbr: st.abbr[0],
                            latitude: st.gtfs_latitude[0],
                            longitude: st.gtfs_longitude[0],
                            address: st.address[0],
                            city: st.city[0],
                            county: st.county[0],
                            state: st.state[0],
                            zipcode: st.zipcode[0],
                        }
                    }));
                }, handleErr => callback(handleErr));
            }
        });
    }

    /**
     * Get station detail from abbr
     * @param station, station abbr
     * @param callback, function(err, stationDetail){}
     */
    station(station, callback) {
        const url = `${this.options.apiHost}/api/stn.aspx?cmd=stninfo&key=${this.options.key}&orig=${station}`;
        request(url, (err, resp, body) => {
            if (err) callback(err);
            else {
                this._parseXML(body, xml => {
                    callback(err, xml.stations[0].station.map(st => {
                        return {
                            name: st.name[0],
                            abbr: st.abbr[0],
                            latitude: st.gtfs_latitude[0],
                            longitude: st.gtfs_longitude[0],
                            address: st.address[0],
                            city: st.city[0],
                            county: st.county[0],
                            state: st.state[0],
                            zipcode: st.zipcode[0],
                            link: st.link[0],
                        }
                    })[0]);
                }, handleErr => callback(handleErr));
            }
        });
    }

    estimate(station, callback) {
        const url = `${this.options.apiHost}/api/etd.aspx?cmd=etd&key=${this.options.key}&orig=${station}`;
        request(url, (err, resp, body) => {
            if (err) callback(err);
            else {
                this._parseXML(body, xml => {
                    const ret = {
                        date: xml.date[0],
                        time: xml.time[0],
                        etds: xml.station[0].etd.map(etd => {
                            return {
                                destination: etd.destination[0],
                                abbreviation: etd.abbreviation[0],
                                limited: etd.limited[0],
                                estimates: etd.estimate.map(estimate => {
                                    return {
                                        minutes: estimate.minutes[0],
                                        platform: estimate.platform[0],
                                        direction: estimate.direction[0],
                                        length: estimate.length[0],
                                        hexcolor: estimate.hexcolor[0],
                                        color: estimate.color[0],
                                        bikeflag: estimate.bikeflag[0],
                                    }
                                })
                            }
                        })
                    };

                    callback(err, ret);
                }, handleErr => callback(handleErr));
            }
        });
    }

    estimateWithDirection(station, direction, callback) {
        const url = `${this.options.apiHost}/api/etd.aspx?cmd=etd&key=${this.options.key}&orig=${station}&dir=${direction}`;
        request(url, (err, resp, body) => {
            if (err) callback(err);
            else {
                this._parseXML(body, xml => {
                    const ret = {
                        date: xml.date[0],
                        time: xml.time[0],
                        etds: xml.station[0].etd.map(etd => {
                            return {
                                destination: etd.destination[0],
                                abbreviation: etd.abbreviation[0],
                                limited: etd.limited[0],
                                estimates: etd.estimate.map(estimate => {
                                    return {
                                        minutes: estimate.minutes[0],
                                        platform: estimate.platform[0],
                                        direction: estimate.direction[0],
                                        length: estimate.length[0],
                                        hexcolor: estimate.hexcolor[0],
                                        color: estimate.color[0],
                                        bikeflag: estimate.bikeflag[0],
                                    }
                                })
                            }
                        })
                    };

                    callback(err, ret);
                }, handleErr => callback(handleErr));
            }
        });
    }

    _schedule(options, callback) {
        const {cmd, key, orig, dest, time, date, b, a, l} = Object.assign({}, options);
        const url = `${this.options.apiHost}/api/sched.aspx?cmd=${cmd}&key=${key}&orig=${orig}&dest=${dest}&time=${time}&date=${date}&b=${b}&a=${a}&l=${l}`;
        request(url, (err, resp, body) => {
            if (err) callback(err);
            else {
                this._parseXML(body, xml => {
                    const ret = {
                        origin: xml.origin[0],
                        destination: xml.destination[0],
                        sched_num: xml.sched_num[0],
                        schedule: xml.schedule.map(sched => {
                            return {
                                date: sched.date[0],
                                time: sched.time[0],
                                before: sched.before[0],
                                after: sched.after[0],
                                trips: sched.request[0].trip.map(trip => {
                                    return {
                                        origin: trip.$.origin,
                                        destination: trip.$.destination,
                                        fare: trip.$.fare,
                                        origTimeMin: trip.$.origTimeMin,
                                        origTimeDate: trip.$.origTimeDate,
                                        destTimeMin: trip.$.destTimeMin,
                                        destTimeDate: trip.$.destTimeDate,
                                        clipper: trip.$.clipper,
                                        tripTime: trip.$.tripTime,
                                        co2: trip.$.co2,
                                        legs: trip.leg.map(leg => {
                                            return {
                                                order: leg.$.order,
                                                transfercode: leg.$.transfercode,
                                                origin: leg.$.origin,
                                                destination: leg.$.destination,
                                                origTimeMin: leg.$.origTimeMin,
                                                origTimeDate: leg.$.origTimeDate,
                                                destTimeMin: leg.$.destTimeMin,
                                                destTimeDate: leg.$.destTimeDate,
                                                line: leg.$.line,
                                                bikeflag: leg.$.bikeflag,
                                                trainHeadStation: leg.$.trainHeadStation,
                                                trainId: leg.$.trainId,
                                                trainIdx: leg.$.trainIdx,
                                            }
                                        }),
                                        fares: {
                                            level: trip.fares[0].$.level,
                                            fares: trip.fares[0].fare.map(fare => {
                                                return {
                                                    amount: fare.$.amount,
                                                    "class": fare.$.class
                                                }
                                            })
                                        }
                                    }
                                })
                            }
                        }),
                    };
                    callback(err, ret);
                }, handleErr => callback(handleErr));
            }
        })
    }

    arriveSchedule(orig, dest, callback, time = 'now', date = 'today', b = 2, a = 2, l = 0) {
        this._schedule({
            cmd: 'arrive',
            key: this.options.key,
            orig,
            dest,
            time,
            date,
            b,
            a,
            l,
        }, callback);
    }

    departSchedule(orig, dest, callback, time = 'now', date = 'today', b = 2, a = 2, l = 0) {
        this._schedule({
            cmd: 'depart',
            key: this.options.key,
            orig,
            dest,
            time,
            date,
            b,
            a,
            l,
        }, callback);
    }
}

export function createClient(options) {
    return new BartClient(options);
}