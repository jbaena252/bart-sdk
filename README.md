BART API Client SDK
===================

[![Build Status](https://travis-ci.org/zgwmike/bart-sdk.svg?branch=master)](https://travis-ci.org/zgwmike/bart-sdk)

[![NPM](https://nodei.co/npm/bart-sdk.png)](https://nodei.co/npm/bart-sdk/)

> it helps to consume bart API easier.

## Install

    $ npm install bart-sdk

## API

```js
const client = require('bart').createClient();
```

Returns a `BartClient` object

### `BartClient`

#### `stations`
```js
client.stations((err, stations)=>{console.dir(stations});
/*
result:
[
  {
    "name": "12th St. Oakland City Center",
    "abbr": "12TH",
    "latitude": "37.803664",
    "longitude": "-122.271604",
    "address": "1245 Broadway",
    "city": "Oakland",
    "county": "alameda",
    "state": "CA",
    "zipcode": "94612"
  },
  {
    "name": "16th St. Mission",
    "abbr": "16TH",
    "latitude": "37.765062",
    "longitude": "-122.419694",
    "address": "2000 Mission Street",
    "city": "San Francisco",
    "county": "sanfrancisco",
    "state": "CA",
    "zipcode": "94110"
  },
  ...
]
*/
```


## Tests

Tests included use [Mocha](http://mochajs.org/). Use `npm test` to run the tests

    $ npm test

# Contributing

Contributions to the project are welcome. Feel free to fork and improve. I accept pull requests when tests are included.
