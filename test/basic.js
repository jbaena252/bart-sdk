const assert = require('assert');
const bart = require('../src/bart');
const client = bart.createClient();

describe("Bart API Client", function () {

    it('should return error when key is invalid', (done) => {
        bart.createClient({key: 'INVALID_KEY'}).stations((err, body) => {
            assert(err);
            assert(err.text === 'Invalid key');
            assert(err.details === 'The api key was missing or invalid.');
            done();
        });
    });
    it('should read stations data', (done) => {
        client.stations((err, body) => {
            assert(body.length > 40);
            body.forEach(st => {
                assert(st.name !== '');
                assert(st.abbr !== '');
                assert(st.latitude !== '');
                assert(st.longitude !== '');
                assert(st.address !== '');
                assert(st.city !== '');
                assert(st.county !== '');
                assert(st.state !== '');
                assert(st.zipcode !== '');
            });
            done();
        });
    });
    it('should return station info', (done) => {
        client.station('lafy', (err, station) => {
            assert(station);
            assert(station.abbr.toLowerCase() === 'lafy');
            assert(station.city !== '');
            assert(station.link !== '');
            done();
        })
    });
    it("should return error for station when station abbr is wrong", function (done) {
        client.station('foobar', (err, station) => {
            assert(station === undefined);
            assert(err.text === 'Invalid orig');
            assert(err.details === 'The orig station parameter FOOBAR is missing or invalid.');
            done();
        })
    });
    it("should return estimates for station", done => {
        client.estimate('lafy', (err, est) => {
            assert(!err);
            assert(est);
            assert(est.date !== '');
            assert(est.time !== '');
            assert(est.etds.length > 0);
            assert(est.etds[0].destination !== '');
            assert(est.etds[0].abbreviation !== '');
            assert(est.etds[0].limited !== '');
            assert(est.etds[0].estimates.length > 0);
            assert(est.etds[0].estimates[0].minutes !== '');
            assert(est.etds[0].estimates[0].platform !== '');
            assert(est.etds[0].estimates[0].direction !== '');
            assert(est.etds[0].estimates[0].length !== '');
            assert(est.etds[0].estimates[0].hexcolor !== '');
            assert(est.etds[0].estimates[0].color !== '');
            assert(est.etds[0].estimates[0].bikeflag !== '');
            done();
        });
    });

    it("should return error for estimate when station abbr is wrong", (done) => {
        client.estimate('foobar', (err, station) => {
            assert(station === undefined);
            assert(err.text === 'Invalid orig');
            assert(err.details === 'The orig station parameter FOOBAR is missing or invalid.');
            done();
        })
    });

    it("should return arrive schedule for orig and dest", done => {
        const orig = 'LAFY', dest = '19TH';
        client.arriveSchedule(orig, dest, (err, sche) => {
            assert(!err);
            assert(sche);
            assert(sche.origin != '');
            assert(sche.destination != '');
            assert(sche.sched_num != '');
            assert(sche.schedule.length > 0);
            assert(sche.schedule[0].date != '');
            assert(sche.schedule[0].time != '');
            assert(sche.schedule[0].before === '2');
            assert(sche.schedule[0].after === '2');
            assert(sche.schedule[0].trips.length === 4);
            assert(sche.schedule[0].trips[0].origin === orig);
            assert(sche.schedule[0].trips[0].destination === dest);
            assert(sche.schedule[0].trips[1].origin === orig);
            assert(sche.schedule[0].trips[1].destination === dest);
            assert(sche.schedule[0].trips[2].origin === orig);
            assert(sche.schedule[0].trips[2].destination === dest);
            assert(sche.schedule[0].trips[3].origin === orig);
            assert(sche.schedule[0].trips[3].destination === dest);
            assert(sche.schedule[0].trips[0].legs.length === 1);
            assert(sche.schedule[0].trips[0].fares.fares.length > 0);
            sche.schedule[0].trips[0].legs.forEach(leg => {
                assert(leg.order != '');
                assert(leg.origin != '');
                assert(leg.destination != '');
                assert(leg.origTimeMin != '');
                assert(leg.origTimeDate != '');
                assert(leg.destTimeMin != '');
                assert(leg.destTimeDate != '');
                assert(leg.line != '');
                assert(leg.bikeflag != '');
                assert(leg.trainHeadStation != '');
                assert(leg.trainId != '');
                assert(leg.trainIdx != '');
            });
            assert(sche.schedule[0].trips[0].fares.level != '');
            sche.schedule[0].trips[0].fares.fares.forEach(fare => {
                assert(fare.amount != '');
                assert(fare.class != '');
            });
            done();
        });
    });
    it("should return error for arrive schedule with invalid orig", done => {
        const orig = 'foo', dest = 'lafy';
        client.arriveSchedule(orig, dest, (err, schedule) => {
            assert(err);
            assert(err.text === 'Invalid orig');
            assert(err.details === 'The orig station parameter FOO is missing or invalid.');
            assert(!schedule);
            done();
        });
    });
    it("should return error for arrive schedule with invalid dest", done => {
        const orig = '12th', dest = 'foo';
        client.arriveSchedule(orig, dest, (err, schedule) => {
            assert(err);
            assert(err.text === 'Invalid dest');
            assert(err.details === 'The dest station parameter FOO is missing or invalid.');
            assert(!schedule);
            done();
        });
    });

    it("should return depart schedule for orig and dest", done => {
        const orig = 'LAFY', dest = '19TH';
        client.departSchedule(orig, dest, (err, sche) => {
            assert(!err);
            assert(sche);
            assert(sche.origin != '');
            assert(sche.destination != '');
            assert(sche.sched_num != '');
            assert(sche.schedule.length > 0);
            assert(sche.schedule[0].date != '');
            assert(sche.schedule[0].time != '');
            assert(sche.schedule[0].before === '2');
            assert(sche.schedule[0].after === '2');
            assert(sche.schedule[0].trips.length === 4);
            assert(sche.schedule[0].trips[0].origin === orig);
            assert(sche.schedule[0].trips[0].destination === dest);
            assert(sche.schedule[0].trips[1].origin === orig);
            assert(sche.schedule[0].trips[1].destination === dest);
            assert(sche.schedule[0].trips[2].origin === orig);
            assert(sche.schedule[0].trips[2].destination === dest);
            assert(sche.schedule[0].trips[3].origin === orig);
            assert(sche.schedule[0].trips[3].destination === dest);
            assert(sche.schedule[0].trips[0].legs.length === 1);
            assert(sche.schedule[0].trips[0].fares.fares.length > 0);
            sche.schedule[0].trips[0].legs.forEach(leg => {
                assert(leg.order != '');
                assert(leg.origin != '');
                assert(leg.destination != '');
                assert(leg.origTimeMin != '');
                assert(leg.origTimeDate != '');
                assert(leg.destTimeMin != '');
                assert(leg.destTimeDate != '');
                assert(leg.line != '');
                assert(leg.bikeflag != '');
                assert(leg.trainHeadStation != '');
                assert(leg.trainId != '');
                assert(leg.trainIdx != '');
            });
            assert(sche.schedule[0].trips[0].fares.level != '');
            sche.schedule[0].trips[0].fares.fares.forEach(fare => {
                assert(fare.amount != '');
                assert(fare.class != '');
            });
            done();
        });
    });
    it("should return error for depart schedule with invalid orig", done => {
        const orig = 'foo', dest = 'lafy';
        client.departSchedule(orig, dest, (err, schedule) => {
            assert(err);
            assert(err.text === 'Invalid orig');
            assert(err.details === 'The orig station parameter FOO is missing or invalid.');
            assert(!schedule);
            done();
        });
    });
    it("should return error for depart schedule with invalid dest", done => {
        const orig = '12th', dest = 'foo';
        client.departSchedule(orig, dest, (err, schedule) => {
            assert(err);
            assert(err.text === 'Invalid dest');
            assert(err.details === 'The dest station parameter FOO is missing or invalid.');
            assert(!schedule);
            done();
        });
    });
});